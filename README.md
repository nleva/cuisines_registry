# Cuisines Registry

## The story:

Cuisines Registry is an important part of Book-That-Table Inc. backend stack. It keeps in memory customer preferences for restaurant cuisines and is accessed by a bunch of components to register and retrieve data. 


The first iteration of this component was implemented by rather inexperienced developer and now may require some cleaning while new functionality is being added. But fortunately, according to his words: "Everything should work and please keep the test coverage as high as I did"


## Your tasks:
All tasks can be found [HERE](https://gitlab.com/nleva/cuisines_registry/issues?scope=all&utf8=%E2%9C%93&state=all)
1. **[Important!]** Adhere to the boy scout rule. Leave your code better than you found it.
It is ok to change any code as long as the CuisinesRegistry interface remains unchanged.
2. Make is possible for customers to follow more than one cuisine (return multiple cuisines in de.quandoo.recruitment.registry.api.CuisinesRegistry#customerCuisines)
3. Implement de.quandoo.recruitment.registry.api.CuisinesRegistry#topCuisines - returning list of most popular (highest number of registered customers) ones
4. Create a short write up on how you would plan to scale this component to be able process in-memory billions of customers and millions of cuisines (Book-That-Table is already planning for galactic scale). (100 words max) [ 100 words issue CLICK ME ](https://gitlab.com/nleva/cuisines_registry/issues/3)

## Lombok IDE setup
IDE needs plugin to be installed. [lombok project setup instructions](https://projectlombok.org/setup/overview)

## Submitting your solution

+ Fork it to a private gitlab repository.
+ Put write up mentioned in point 4. into this file. [ 100 words issue CLICK ME ](https://gitlab.com/nleva/cuisines_registry/issues/3)
+ Send us a link to the repository, together with private ssh key that allows access (settings > repository > deploy keys).