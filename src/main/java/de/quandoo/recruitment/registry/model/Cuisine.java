package de.quandoo.recruitment.registry.model;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Cuisine pojo
 */
@Builder
@Getter
@EqualsAndHashCode
public class Cuisine {
    String name;

    public static class CuisineBuilder{
        static Map<String,Cuisine> cuisineMap = new ConcurrentHashMap<>();

        public Cuisine build(){
            Cuisine cuisine = cuisineMap.get(this.name);
            if(cuisine==null){
                synchronized (cuisineMap){
                    cuisine = cuisineMap.get(this.name);
                    if(cuisine==null){
                        cuisine=new Cuisine(this.name);
                        cuisineMap.put(this.name, cuisine);
                    }
                }
            }

            return cuisine;
        }
    }
}
