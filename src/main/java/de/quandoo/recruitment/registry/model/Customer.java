package de.quandoo.recruitment.registry.model;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Customer model
 */
@Builder
@Getter
@EqualsAndHashCode
public class Customer {
    String uuid;


    public static class CustomerBuilder{
        static Map<String,Customer> customerMap = new ConcurrentHashMap<>();

        public Customer build(){
            Customer customer = customerMap.get(this.uuid);
            if(customer==null){
                synchronized (customerMap){
                    customer = customerMap.get(this.uuid);
                    if(customer==null){
                        customer=new Customer(this.uuid);
                        customerMap.put(this.uuid, customer);
                    }
                }
            }

            return customer;
        }
    }
}
