package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import lombok.NonNull;
import lombok.extern.flogger.Flogger;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * CuisinesRegistry implementation
 * {@inheritDoc}
 * //TODO singleton
 */
@Flogger
public class InMemoryCuisinesRegistry implements CuisinesRegistry {


    Map<Cuisine, Set<Customer>> cuisineCustomersMap = new ConcurrentHashMap<>();
    Map<Customer, Set<Cuisine>> customersCuisineMap = new ConcurrentHashMap<>();

    TreeMap<Integer, Set<Cuisine>> top = new TreeMap<>((o1, o2) -> -1 * o1.compareTo(o2));

    /**
     * {@inheritDoc}
     * @param customer
     * @param cuisine
     */
    @Override
    public void register(@NonNull  Customer customer, @NonNull Cuisine cuisine) {

        Set<Customer> customers = getOrCreate(cuisine, cuisineCustomersMap);
        Set<Cuisine> cuisines = getOrCreate(customer, customersCuisineMap);

        customers.add(customer);
        cuisines.add(cuisine);

        updateTop(cuisine, customers);
    }

    private void updateTop(@NonNull Cuisine cuisine, @NonNull Set<Customer> customers) {
        int customersCount = customers.size();

        Set<Cuisine> cusinesByCount = getOrCreateCuisinesByCount(customersCount);
        cusinesByCount.add(cuisine);

        if(customersCount>1){
            top.get(customersCount-1).remove(cuisine);
        }
    }

    private Set<Cuisine> getOrCreateCuisinesByCount(int customersCount) {
        Set<Cuisine> cusinesByCount = top.get(customersCount);
        if (cusinesByCount == null) {
            synchronized (top){
                cusinesByCount = top.get(customersCount);
                if (cusinesByCount == null) {
                    cusinesByCount = Collections.newSetFromMap(new ConcurrentHashMap<>());
                    top.put(customersCount,cusinesByCount);
                }
            }
        }
        return cusinesByCount;
    }

    private static <K, V> Set<V> getOrCreate(K key, Map<K, Set<V>> map) {
        Set<V> set = map.get(key);
        if (set == null) {
            synchronized (map) {
                set = map.get(key);
                if (set == null) {
                    set = Collections.newSetFromMap(new ConcurrentHashMap<>());
                    map.put(key, set);
                }
            }
        }
        return set;
    }

    /**
     * {@inheritDoc}
     * @param cuisine
     * @return
     */
    @Override
    public List<Customer> cuisineCustomers( Cuisine cuisine) {
        return getList(cuisine, cuisineCustomersMap);
    }

    /**
     * {@inheritDoc}
     * @param customer
     * @return
     */
    @Override
    public List<Cuisine> customerCuisines( Customer customer) {
        return getList(customer, customersCuisineMap);
    }

    private static <K, V> List<V> getList(K key, Map<K, Set<V>> cuisineCustomersMap) {
        List<V> list = new ArrayList<>();
        if(key==null) {
            log.atInfo().log("Argument is null. returning empty list");
            return list;
        }
        Set<V> set = cuisineCustomersMap.get(key);
        if (set != null) {
            list.addAll(set);
        }
        return list;
    }

    /**
     * {@inheritDoc}
     * @param n limit
     * @return
     */
    @Override
    public List<Cuisine> topCuisines(int n) {
        if(n<=0){
            log.atInfo().log("topCuisines limit is set to %d. Set limit to positive value.",n);
            return Collections.emptyList();
        }

        return getTopCuisineStream(n)
                .distinct()
                .limit(n)
                .collect(Collectors.toList());
    }

    private Stream<Cuisine> getTopCuisineStream(int n) {
        Stream<Cuisine> cuisineStream = Stream.empty();

        int tmp = n;
        for (Map.Entry<Integer, Set<Cuisine>> entry : top.entrySet()) {
            Set<Cuisine> cuisineSet = entry.getValue();
            cuisineStream = Stream.concat(cuisineStream,cuisineSet.stream());
            if((tmp-=cuisineSet.size())<=0){
                break;
            }
        }
        return cuisineStream;
    }

}
