package de.quandoo.recruitment.registry.api;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.List;

/**
 * Cuisine Reqistry provides operations with customers and cuisines
 */
public interface CuisinesRegistry {

    /**
     * Register costomer in cuisine
     * @param customer
     * @param cuisine
     */
    void register(Customer customer, Cuisine cuisine);

    /**
     * Get list of customers cuisines
     * @param customer
     * @return list of cuisines of empty list
     */
    List<Cuisine> customerCuisines(Customer customer);

    /**
     * Get top N cuisines with max amount of customers
     * @param n limit
     * @return list of cuisines of empty list
     */
    List<Cuisine> topCuisines(int n);

    /**
     * Get customers by cuisine
     * @param cuisine
     * @return list of customers of empty list
     */
    List<Customer> cuisineCustomers(Cuisine cuisine);
}
