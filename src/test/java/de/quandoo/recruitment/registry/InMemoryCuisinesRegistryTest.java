package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import lombok.extern.flogger.Flogger;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;

@Flogger
public class InMemoryCuisinesRegistryTest extends Assert {

    CuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();


    @BeforeTest()
    void initValues(){
        cuisinesRegistry.register(Customer.builder().uuid("1").build(), Cuisine.builder().name("french").build());

        cuisinesRegistry.register(Customer.builder().uuid("2").build(), Cuisine.builder().name("german").build());
        cuisinesRegistry.register(Customer.builder().uuid("3").build(), Cuisine.builder().name("german").build());

        cuisinesRegistry.register(Customer.builder().uuid("4").build(), Cuisine.builder().name("italian").build());
        cuisinesRegistry.register(Customer.builder().uuid("5").build(), Cuisine.builder().name("italian").build());
        cuisinesRegistry.register(Customer.builder().uuid("6").build(), Cuisine.builder().name("italian").build());

        log.atFine().log("registry initialized");
    }

    @DataProvider
    public Object[][] cuisines() {
        return new Object[][]{
                {new String[]{"1"},             new String[]{"french"},                     1, 1},
                {new String[]{"1","1"},         new String[]{"french"},                     1, 1},
                {new String[]{"1"},             new String[]{"french","french"},            1,1},
                {new String[]{"1","2"},         new String[]{"french","french"},            2,1},
                {new String[]{"1","2","3","4"}, new String[]{"french","german","italian"},  4,3},
                {new String[]{"1","2","3","1"}, new String[]{"french","german","french"},   3,2},
        };
    }

    @Test(dataProvider = "cuisines")
    public void testRegistration(String[] customerIds, String[] cuisineNames, int distinctCustomers, int distinctCuisines){

        CuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

        for (String customerId : customerIds) {
            for (String cuisineName : cuisineNames) {
                Customer customer = Customer.builder().uuid(customerId).build();
                Cuisine cuisine = Cuisine.builder().name(cuisineName).build();
                cuisinesRegistry.register(customer, cuisine);
            }
        }
        log.atFine().log("new registry created");

        testDistinctCuisines(customerIds, distinctCuisines, cuisinesRegistry);
        log.atFine().log("Distinct cuisines test OK");

        testDistinctCustomers(cuisineNames, distinctCustomers, cuisinesRegistry);
        log.atFine().log("Distinct customers test OK");

        testTopCuisines(distinctCuisines, cuisinesRegistry);
        log.atFine().log("Top cuisines test OK");

        List<String> customers = cuisinesRegistry
                .cuisineCustomers(Cuisine.builder().name(cuisineNames[0]).build())
                .stream().map(Customer::getUuid).collect(Collectors.toList());
        assertThat("Cuisine should contain every customer",customers, hasItems(customerIds));

        List<String> cuisines = cuisinesRegistry.customerCuisines(Customer.builder().uuid(customerIds[0]).build())
                .stream().map(Cuisine::getName).collect(Collectors.toList());
        assertThat("Customer should be registerd in every cuisine", cuisines, hasItems(cuisineNames));

    }

    private void testTopCuisines(int distinctCuisines, CuisinesRegistry cuisinesRegistry) {
        List<Cuisine> topCuisines = cuisinesRegistry.topCuisines(distinctCuisines * 2);
        assertThat("If arg is higher then number of cuisines then all cuisines should be returned",topCuisines.size(), is(equalTo(distinctCuisines)));
        log.atFine().log("Get all top cuisines test OK");

        topCuisines = cuisinesRegistry.topCuisines(1);
        assertThat("If any customer is registred then topCuisine(1) should return ONE cuisine ",topCuisines.size(), is(equalTo(1)));
        log.atFine().log("Get ONE top cuisine test OK");

        topCuisines = cuisinesRegistry.topCuisines(0);
        assertThat("Shuold return empty list if arg is zero", topCuisines.size(), is(equalTo(0)));
        log.atFine().log("Get ZERO top cuisines test OK");

        topCuisines = cuisinesRegistry.topCuisines(-10);
        assertThat("Should return empty list if arg is negative",topCuisines.size(), is(equalTo(0)));
        log.atFine().log("Get NEGATIVE top cuisines test OK");
    }

    private void testDistinctCustomers(String[] cuisineNames, int distinctCustomers, CuisinesRegistry cuisinesRegistry) {
        for (String cuisineName : cuisineNames) {
            Cuisine cuisine = Cuisine.builder().name(cuisineName).build();
            assertThat("Each customer should be registered to every cuisine",cuisinesRegistry.cuisineCustomers(cuisine).size(), is(equalTo(distinctCustomers)));
        }
    }

    private void testDistinctCuisines(String[] customerIds, int distinctCuisines, CuisinesRegistry cuisinesRegistry) {
        for (String customerId : customerIds) {
            Customer customer = Customer.builder().uuid(customerId).build();
            assertThat("Each ciusine should countain all customers",cuisinesRegistry.customerCuisines(customer).size(), is(equalTo(distinctCuisines)));
        }
    }

    @Test()
    public void cuisineByNull() {
        List<Customer> customers = cuisinesRegistry.cuisineCustomers(null);
        assertThat("Reuslt is empty list, not NULL",customers, is( notNullValue() ));
        assertThat("List should be empty",customers.size(), is(equalTo( 0 )));
        log.atFine().log("Get cuisineCustomers by null test OK");
    }

    @Test(expectedExceptions = RuntimeException.class, expectedExceptionsMessageRegExp = "customer .+ is null")
    public void registerNullCustomer(){
        cuisinesRegistry.register(null, Cuisine.builder().name("test").build());
    }
    @Test(expectedExceptions = RuntimeException.class, expectedExceptionsMessageRegExp = "cuisine .+ is null")
    public void registerNullCuisine(){
        cuisinesRegistry.register(Customer.builder().uuid("0").build(), null);
    }

    @Test
    public void customerByNull() {
        List<Cuisine> cuisines = cuisinesRegistry.customerCuisines(null);
        assertThat("Reuslt is empty list, not NULL", cuisines, is( notNullValue() ));
        assertThat("List should be empty", cuisines.size(), is(equalTo( 0 )));
        log.atFine().log("Get customerCuisines by null test OK");
    }

    @Test(timeOut = 10_000)
    public void manyCustomers() {
        Cuisine testCuisine = Cuisine.builder().name("test").build();
        int totalCustomers = 100_000;
        for (int i = 0; i < totalCustomers; i++) {
            cuisinesRegistry.register(Customer.builder().uuid(""+i).build(), testCuisine);
        }
        assertThat("Top cuisine is test cuisine",cuisinesRegistry.topCuisines(1).get(0),is(equalTo(testCuisine)));
        assertThat("Top cuisine should contain "+totalCustomers +" customers",cuisinesRegistry.cuisineCustomers(testCuisine).size(),is(equalTo(totalCustomers)));
        log.atFine().log("%d customers test OK",totalCustomers);
    }


    private final String test2CuisineName="test2";

    int maxCustomers = 10;
    @Test(threadPoolSize = 10, invocationCount = 100, timeOut = 10_000)
    public void parallelTest(){
        int i = ((int) (Math.random() * maxCustomers));
        cuisinesRegistry.register(Customer.builder().uuid(""+ i).build(), Cuisine.builder().name(test2CuisineName).build());
    }


    @Test(dependsOnMethods = "parallelTest")
    public void parallelTestResult(){
        List<Customer> customers = cuisinesRegistry.cuisineCustomers(Cuisine.builder().name(test2CuisineName).build());
        assertThat("Total amount of customers shouldn't exceed "+maxCustomers, customers.size(), lessThanOrEqualTo(maxCustomers));
        log.atFine().log("parallel test OK");
    }

}
